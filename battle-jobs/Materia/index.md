---
authors:
        - Robin Naite
tags:
    - Materia
---
# What is Materia? When do I worry about it?

Materia is used to increase the sub-stats of gear.

Materia is only important when you've reached max level, and are gearing for the latest gear possible. 

If you have materia laying around feel free to slot it into a gear slot through an NPC melder.

## Questions
- [How to meld materia](How-to-meld-materia.md)
- [How to unlock materia melding](How-to-unlock-materia.md)
- [How to overmeld](How-to-overmeld.md)
- [Where do I get materia](Where-do-I-get-materia.md)
- [What stats to meld](What-stats-to-meld.md)

