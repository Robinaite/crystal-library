---
authors:
    - Robin Naite # Substitute my name
tags: # tags are to help categorize and search for specific stuff easier, as well as different ways to reference to this topic. For more check the guidelines document.
    - stats to meld
    - Materia
---
# What stats do I meld?

## Summary

To find out what stats you should meld check The Balance discord on your job: [https://discord.gg/thebalanceffxiv](https://discord.gg/thebalanceffxiv){target=_blank}
