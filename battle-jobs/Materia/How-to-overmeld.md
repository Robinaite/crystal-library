---
authors:
    - Robin Naite # Substitute my name
tags: # tags are to help categorize and search for specific stuff easier, as well as different ways to reference to this topic. For more check the guidelines document.
    - Overmeld
    - Overmelding
    - Materia
---
# How to overmeld

## Summary


Overmelding is the process of adding more materia than the base slots that the gear has. This is only possible to do on crafted (non-augmented) gear.

- You can ovemeld to up to 5 melds in total. 
- The first slot can be the highest tier, afterwards only lower tiers.
- The more you meld the lower the chance of succes.
- Gear is **not** lost if melding fails, only the materia.

![Overmelded crafters gear, red box shows which materia are overmelded](images/Overmelded-Gear.png)

## Where to overmeld?
You can ovemeld by:
- Having a crafter levelled to overmeld
- Use the ingame Request a meld from someone who has their crafters levelled.

![Right Click on character then select Request Meld](images/Request-Meld-Right-Click.png)




