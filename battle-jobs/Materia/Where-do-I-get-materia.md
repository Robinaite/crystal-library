---
authors:
    - Robin Naite # Substitute my name
tags: # tags are to help categorize and search for specific stuff easier, as well as different ways to reference to this topic. For more check the guidelines document.
    - Obtain Materia
    - Materia
---
# Where do I get Materia?
## Summary

You can get materia from:

- Doing Dungeons (only battle materia)
- Trading in Cracked Clusters obtained from doing roulletes and other means
- From the marketboard
- From White Scrips (Only crafters & gatherers materia)