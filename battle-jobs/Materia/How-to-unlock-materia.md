---
authors:
    - Robin Naite # Substitute my name
tags: # tags are to help categorize and search for specific stuff easier, as well as different ways to reference to this topic. For more check the guidelines document.
    - Unlock Materia
    - Materia
---
# How do I unlock Materia?

## Summary

To unlock Materia you need to do the quest [Forging the Spirit](https://ffxiv.gamerescape.com/wiki/Forging_the_Spirit){target=_blank}.

The quest can be found  at Swynbroes: Central Thanalan - Black Brush - The Bonfire (x:24, y:13.8) 

![](images/Swynbroes-location-Central-Thanalan.png)