---
authors:
    - Robin Naite # Substitute my name
tags: # tags are to help categorize and search for specific stuff easier, as well as different ways to reference to this topic. For more check the guidelines document.
    - Meld Materia
    - Materia
---
# How to meld materia?

## Summary

To meld materia you have three options:

1. Going to an NPC Melder in any of the major cities.
2. Leveling necessary crafters
3. Use Request meld option on someone who has crafters levelled

![Right Click on character then select Request Meld](images/Request-Meld-Right-Click.png)


