---
authors:
        - Reiko
tags:
    - GCD
    - oGCD
    - Global Cooldown
    - Weaving
---
# What are GCD and oGCDs?

## Global Cooldown (GCD)

The Global Cooldown (GCD) is the 2.5 seconds or less (reduced by the Skill/Spell Speed stat ) cooldown timer that is triggered upon the use of Weaponskills for physical classes, and Spells for magic classes. 

This cooldown period is referred to by the in-game tooltips as your "Recast Time". These skills as a group are more commonly referred to in the community simply as "{{ GCD }}*s*". 

You will **not** be able to use any other {{ GCD }} while the Global Cooldown is going. 

## Off Global Cooldowns
"Off Global Cooldowns" aka {{ oGCD }} are generally referred to as "Abilities" by the game's tooltips and these skills may be used while the Global Cooldown is going, and indeed this is when you would want to use these skills. 

The use of {{ oGCD }}*s* between {{ GCD }}*s* is referred to commonly as "**weaving**". Note there are special exceptions where an Ability will also trigger the {{ GCD }}, however these exceptions are noted in the tooltips of the skill itself.

For more information on how GCD and oGCD are affected by Skill/Spell speed stats check here: [https://www.akhmorning.com/allagan-studies/stats/speed/#skill--spell-speed-stat-tiers](https://www.akhmorning.com/allagan-studies/stats/speed/#skill--spell-speed-stat-tiers)

## Additional Notes

While most {{ GCD }}*s* have the same cooldown timer as the {{ GCD }} itself, this is not always the case and you may have {{ GCD }}*s* that have their own, longer timers. Besides the aforementioned exceptions, most {{ oGCD }}/Abilities have independent cooldowns and are often much longer than the {{ GCD }} though some are actually shorter (these usually use up some kind of resource so they aren't spammable). There are also times when an {{ oGCD }} will share a cooldown with another {{ oGCD }} as well, but these will be noted in the tooltip.

When it comes to weaving your {{ oGCD }} between your {{ GCD }}*s*, bare in mind that all skills, no matter what, have a minimum of a .7 second delay before another skill will go off. This puts a hard limit on weaving {{ oGCD }} at 2 weaves aka a double weave per Global Cooldown window. Bare in mind though that either due to a longer delay (like {{DRG}}*'s* jumps/dives), a shortened {{ GCD }} (like {{MCH}}*'s* hypercharge), or a combination of a faster than average global cooldown and bad ping, it may only be practical to single weave during a given {{ GCD }}. No matter the case, you want to minimize delaying your next {{ GCD }} skill which can be caused by over or late weaving. Also note that {{ oGCD }} that are macroed, as well as non-queued actions like Sprint or using a potion, can only ever be single weaved and must be initiated as soon as possible in order not to delay your next {{ GCD }} cast.

Most of this description has been assuming that your {{ GCD }} skill goes off instantly (not counting the .7sec delay). Of course, this is not always the case for casters and healers. And indeed, they need to be conscious of the fact that a "weave window" (i.e. a triggering of the {{ GCD }} that gives time enough to weave at least 1 {{ oGCD }}) only occurs when they have an insta-cast skill or one that is at least about 1 sec faster than the {{ GCD }} recast timer. Otherwise, if you attempt to use an {{ oGCD }} after a cast skill that is the same time or longer than your {{ GCD }} recast timer, you will always delay your next {{ GCD }} cast.
