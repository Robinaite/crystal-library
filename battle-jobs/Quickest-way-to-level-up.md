---
authors:
        - Robin Naite
tags:
    - Level Up
---
# What is the quickest way to level up?

## Short Answer

The quickest way to level up is your MSQ and Job Quests, if you have not done those yet, for all jobs.
Until level 15, Hunting Log definitely helps.
For Healer & Tanks the quickest way to level is through daily roulettes and highest possible level dungeon.
For DPS the quickest way to level is either:

- Highest Level Dungeon + Fates
- Squadron Command Missions
- Trusts (in ShB)
- POTD/HoH

More of a video person? Check Zeplas Video about it!

{{yt("uK5Oj3nPgMU")}}
## Longer Answer

There are various ways to level up, here is a non-exhaustive list of possibilities:

- MSQ
- Duty Roulletes
- Highest Level Dungeon
- Bozja Areas (only starting level 71)
- POTD/HoH (Check [here](../content/deep-dungeons/index.md) for more information)
- Squadron Command Missions (until level 50)
- Trusts (only in ShB)
- Fates
- Hunting Log
- Hunts
- PvP

### XP Buffs

- Food: +3% XP
- Armory Bonus: +100% XP on class/jobs lower than your highest job (Lowers to 50% in the last 10 levels)
- Rested XP: +50% XP for a level and a half. Log out in areas where you see a Crescent Moon on your XP Bar!
- FC XP Buff or Squadron Battle Manual: +15% (they are mutually exclusive, either one or the other)
- Preferred Server: Road to (Level Cap-10) buff, +100% xp from all sources including quest XP.
- Novice Hall Ring: +30% XP under Level 30
- Recruit-a-friend Hat: +20% under level 25
- Pre-order Earrings: Each expansion has pre-order earrings which gives XP buff until Level Cap of that expansion minus 10.
- Bozjan Earring: +10% XP between levels 71 and 80.

### I am level 1, no dungeons what do I do?

You can do the following to level up:

- Fates
- Hunting Log
- At Level 10 Guildheists Roulette
