---
authors:
        - Robin Naite
tags:
    - Sage
    - SGE
---
# Sage

## What is a Sage?

Dubbed somanoutics, the art seeks to heal and protect by manipulating corporeal aether through the use of nouliths, a flying array of foci. Practitioners are dedicated to the betterment of mankind, and for their wisdom and compassion, eventually came to be known simply as sages.

Sage ({{ SGE }}) is a Healer.

Sage uses a nouliths as their weapon.

## How to unlock Reaper

1. Own Endwalker expansion
2. Level any other job to 70
3. Start the quest [Sage's Path](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/a8635882d27/){target=_blank} in Limsa Lominsa Lower Decks.
4. Reaper starts at level 70.

## Leveling

Here is a great guide on tips and rotations during leveling:
{{yt("0JHi_iaTcnA")}}

## Endgame BiS and Rotation

For endgame best in slot gear and rotation, head over to The Balance discord server you can find here: [https://discord.gg/thebalanceffxiv](https://discord.gg/thebalanceffxiv){target=_blank}

