---
icon: material/sword
---
# Battle Jobs

In this category you can find all information regarding FFXIV Battle jobs, including how to unlock, questions, and general gameplay content.

=== "Melee DPS"

    <div class="grid cards" markdown>

    -  [![](Images/drg.png){ .middle } Dragoon](DPS/melee-dps/dragoon/index.md)
    -  [![](Images/mnk.png){ .middle } Monk](DPS/melee-dps/monk/index.md)
    -  [![](Images/nin.png){ .middle } Ninja](DPS/melee-dps/ninja/index.md)
    -  [![](Images/rpr.png){ .middle } Reaper](DPS/melee-dps/reaper/index.md)
    -  [![](Images/sam.png){ .middle } Samurai](DPS/melee-dps/samurai/index.md)

    </div>

=== "Ranged DPS"

    <div class="grid cards" markdown>

    -  [![](Images/brd.png){ .middle } Bard](DPS/physical-ranged-dps/bard/index.md)
    -  [![](Images/dnc.png){ .middle } Dancer](DPS/physical-ranged-dps/dancer/index.md)
    -  [![](Images/mch.png){ .middle } Machinist](DPS/physical-ranged-dps/machinist/index.md)

    </div>

=== "Magical DPS"

    <div class="grid cards" markdown>

    -  [![](Images/blm.png){ .middle } Black Mage](DPS/magic-ranged-dps/black-mage/index.md)
    -  [![](Images/rdm.png){ .middle } Red Mage](DPS/magic-ranged-dps/red-mage/index.md)
    -  [![](Images/smn.png){ .middle } Summoner](DPS/magic-ranged-dps/summoner/index.md)

    </div>

=== "Tanks"

    <div class="grid cards" markdown>

    -  [![](Images/drk.png){ .middle } Dark Knight](Tanks/dark-knight/index.md)
    -  [![](Images/gnb.png){ .middle } Gunbreaker](Tanks/gunbreaker/index.md)
    -  [![](Images/pld.png){ .middle } Paladin](Tanks/paladin/index.md)
    -  [![](Images/war.png){ .middle } Warrior](Tanks/warrior/index.md)

    </div>

=== "Healers"

    <div class="grid cards" markdown>

    -  [![](Images/ast.png){ .middle } Astrologian](Healers/astrologian/index.md)
    -  [![](Images/sge.png){ .middle } Sage](Healers/sage/index.md)
    -  [![](Images/sch.png){ .middle } Scholar](Healers/scholar/index.md)
    -  [![](Images/whm.png){ .middle } White Mage](Healers/white-mage/index.md)

    </div>

## Commonly asked questions

{% for subPage in page.parent.children %}
{% if subPage.file is defined %}
{% if subPage.file.url.split('/')[1]|length %}
- [{{ subPage.file.url.split('/')[1].capitalize()|replace('-',' ') }}](../{{subPage.url}})
{% endif %}
{% endif %}
{% endfor %}