---
authors:
        - Robin Naite
tags:
    - Battle Macros
    - Macros
---
# Why minimize Battle Macros usage

## Short Answer

Macros have inherent issues in battle, and will cause DPS loss if used in excessive manners, especially do **NOT**:

- Bind abilities together

What you can do:

- Put Ground target skills on <t\>
- astrologian Cards automatically to party members with <1>

The reason for discouraging macros in battle is due to its limitations, using macros are:

- Error prone
- Does not work with the Skills Queue system
- Wait timers only in Integers (so no 2.5 wait timers, only 2 or 3)

For a quick rundown on the possibilities watch the following video from Akhmorning:

{{ yt("IBrO-ultpnw") }}

For a more in-depth video tutorial I suggest this one from Mr.Happy:  

{{ yt("qshNKeDENlk") }}

## Extra Info

Macros are useful for some situations, especially crafting, but that is not for this question to answer.
