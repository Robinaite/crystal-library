---
authors:
        - Robin Naite
tags:
    - Samurai
    - SAM
---
# Samurai

## What is a Samurai?

And taking to battle in their lieges' names were noble swordsmen whose art was forged in the crucible of war: the samurai.

Samurai ({{ SAM }}) is a Melee DPS.

Samurai uses a katana as their Weapon.

## How to unlock Samurai

1. Own the Stormblood Expansion
2. Level any other Job to level 50
3. Start the quest [The Way of the Samurai](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/6b274c524ae/){target=_blank} in Ul'dah
4. Samurai starts at level 50

## Leveling

Here is a great guide on tips and rotations during leveling:
{{yt("CmPGIYBMd-g")}}

## Endgame BiS and Rotation

For endgame best in slot gear and rotation, head over to The Balance discord server you can find here: [https://discord.gg/thebalanceffxiv](https://discord.gg/thebalanceffxiv){target=_blank}
