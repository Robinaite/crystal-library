---
authors:
        - Robin Naite
tags:
    - Ninja
    - NIN
    - RGE
    - Rogue
---
# Ninja

## What is a Ninja?

Hailing from the war-torn lands of the Far East, the secret arts of the ninja were born of necessity, and have since given rise to a unique breed of highly-trained combatants.

Ninja ({{ NIN }}) is a Melee DPS.

Ninja uses two daggers as their weapon.

## How to unlock Ninja

1. Finish level 10 class quest of any class.
2. Unlock Rogue in Limsa Lominsa, which starts at level 1 ([My First Daggers](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/206f120e683/){target=_blank})
3. Level up Rogue to level 30
4. Unlock {{ NIN }} through doing the job quests. ([Peasants by Day, Ninjas by Night](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/fb587e6e821/){target=_blank})

Make sure you have done {{ MSQ }} level 20 quest, Sylph Management. For more info check here: [Advancement Job Quest does not appear](../../../Unlock-job-at-level-30.md)

## Leveling

**Outdated as of Endwalker launch, as no alternatives still here. Watch with a grain of salt.**
Here is a great guide on tips and rotations during leveling:
{{yt("ygNG68HJjko")}}

## Endgame BiS and Rotation

For endgame best in slot gear and rotation, head over to The Balance discord server you can find here: [https://discord.gg/thebalanceffxiv](https://discord.gg/thebalanceffxiv){target=_blank}
