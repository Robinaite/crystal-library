---
authors:
        - Reiko
        - Robin Naite
tags:
    - Dragoon
    - DRG
    - LNC
    - Lancer
---
# Dragoon

## What is a Dragoon?

Dragoons, Born amidst the timeless conflict between men and dragons, these lance-wielding knights have developed an aerial style of combat, that they might better pierce the scaled hides of their mortal foes.

Dragoon ({{ DRG }}) is a Melee DPS which is highly mobile (lots of jumps back and forth).

Dragoon uses a polearm as their weapon.

## How to unlock Dragoon

1. Unlock Lancer in Old Gridania, which starts at level 1 ([Way of the Lancer](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/f5a62b54de4/){target=_blank})
2. Level up Lancer to level 30
3. Unlock {{ DRG }} through doing the job quests. ([Eye of the Dragon](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/0533230f632/){target=_blank})

Make sure you have done {{ MSQ }} level 20 quest, Sylph Management. For more info check here: [Advancement Job Quest does not appear](../../../Unlock-job-at-level-30.md)

## Leveling

**Outdated as of Endwalker launch, as no alternatives still here. Watch with a grain of salt.**
Here is a great guide on tips and rotations during leveling:
{{yt("_S-UuGmLPe4")}}

## Endgame BiS and Rotation

For endgame best in slot gear and rotation, head over to The Balance discord server you can find here: [https://discord.gg/thebalanceffxiv](https://discord.gg/thebalanceffxiv){target=_blank}

## Commonly asked questions

### What is Dragoon's playstyle like?

Dragoons are a very balanced DPS, sitting in the middle of personal DPS and party buffs. Its rotation is quite static, going back and forth between two 5 part combos while weaving in numerous off Global Cooldowns. It is a very mobile DPS, having numerous skills that can jump you to, or away from a target. The downside is these do come with animation locks and the simple risk of leaping yourself off the edge of an arena but with a bit of planning and forethought you'll be mastering the dragon within no time.

### Does Dragoon have positionals?

Yes, as with all melee DPS, Dragoon has certain skills that receive extra damage when executed from a certain position. Out of the 4 melee DPS classes, Dragoon in fact has the second most positionals (behind Monk). Your positional skills are Chaos Thrust (Rear), Fang and Claw (Flank), and Wheeling Thrust (Rear). Only three skills but from level 64 onward you will be executing Fang and Claw and Wheeling Thrust both at the end of each of your two main combos. At level 76, hitting your last positional in your combo (whether Fang and Claw or Wheeling Thrust) correctly becomes even more important as not only does it get bonus damage, if done correctly it buffs the damage of your next True Thrust, turning it into Raiden Thrust.

### What's with the "Floor tank" and "loldrg" memes

As mentioned above, some of Dragoon's skills have animation locks where you cannot move while they are executing. It also has a skill that jumps it backwards a fair distance. In the past, the animation locks were *much* longer but now they are quite short and easy to manage. But those coupled with the accidental leap off of an arena's edge has given rise to many a meme. If you are interested in playing the class I would say to simply ignore those. They are mostly of a previous era and have little relevance now.

### What are Dragoon's stat priorities?

While leveling and with rare exception at endgame, your item level should be top priority. At endgame, if not building toward your true Best in Slot, you should prioritize **Critical Hit > Direct Hit = Determination > Skill Speed**. While Skill Speed has the least value, Dragoons can operate at any reasonable skill speed so long as it's not too much. If you're curious what different combos of gear/melds will yield for overall DPS, consult the [Gear Calculator](http://bit.ly/DRG-Gear)

### Why am I not supposed to double weave certain skills?

The aforementioned animation locks also extend the standard lockout time that all skills have before another can be used. Because of this, any time that you weave the skills Jump, High Jump, Spineshatter Dive, Dragonfire Dive, or Stardiver you cannot weave a second skill during that window without delaying your next GCD cast. This isn't really much of an issue, you simply need to be conscious of this fact as you play and delay your next off global cooldown skill till after your next GCD if necessary.

### Who should I give my Dragon Sight to?

When in doubt, simply make sure it goes to someone who will be close to you, which usually means another melee DPS. Regardless of who, you don't want to be fumbling around, delaying your cast of Dragon Sight trying to find an ally who's off who knows where. Optimally though, you want to give it to the teammate who's doing the highest DPS. This could be a lot of different people depending on the players' skill and gear. The one that tends to be of low priority though is Summoner simply due to the annoying quirk that the Dragon Sight damage does not get applied to their pets.
