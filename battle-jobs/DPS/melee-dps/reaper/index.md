---
authors:
        - Robin Naite
tags:
    - Reaper
    - RPR
---
# Reaper

## What is a Reaper?

The Garleans' forebears were once humble farmers who made their home among the sun-drenched fields of southern Ilsabard. That was, until they were driven from this paradise by invaders wielding arcane magicks. Forced north into the frigid mountains, the survivors sought a means to tap into the reservoir of aether otherwise closed to them. A daring few found their answer within the void, binding themselves to its creatures to gain verboten power─power fed by the souls of the slain. Once more they took up their scythes, this time to reap a crimson harvest.

Reaper ({{ RPR }}) is a Melee DPS.

Reaper uses a Scythe as their weapon.

## How to unlock Reaper

1. Own Endwalker expansion
2. Level any other job to 70
3. Start the quest [The Killer Instinct](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/866e56b6974/){target=_blank} in Ul'dah.
4. Reaper starts at level 70.

## Leveling

Here is a great guide on tips and rotations during leveling:
{{yt("DJQKqKWn8Zg")}}

## Endgame BiS and Rotation

For endgame best in slot gear and rotation, head over to The Balance discord server you can find here: [https://discord.gg/thebalanceffxiv](https://discord.gg/thebalanceffxiv){target=_blank}
