---
authors:
        - Robin Naite
tags:
    - Black Mage
    - BLM
    - THM
    - Thaumaturge
---
# Black Mage

## What is a Black mage?

Those who learned to wield this a potent magic of pure destructive force of ruin came to be called black mages, out of both fear and respect for their gift.

Black Mage ({{ BLM }}) is a Magic Ranged DPS, which is often thought of as one of the more complicated jobs in the game, as it requires the player to stand still for as much as possible to cast their explosive spells. It is aided by a bunch of movement abilities(teleports!) to make sure you can stay as long as possible in the same place. Its highly rewarding to get it smoothly going!

Black Mage uses a two handed staff as their weapon.

## How to Unlock Black Mage

1. Unlock Thaumaturge in Ul'dah, which starts at level 1 ([Way of the Thaumaturge](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/eb9c9e119b8/){target=_blank})
2. Level up Thaumaturge to level 30
3. Unlock {{ BLM }} through doing the job quests. ([Taking the Black](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/eec832efc26/){target=_blank})

Make sure you have done {{ MSQ }} level 20 quest, Sylph Management. For more info check here: [Advancement Job Quest does not appear](../../../Unlock-job-at-level-30.md)

## Leveling

Here is a great guide on tips and rotations during leveling:
{{yt("cEm-W92K3kg")}}

## Endgame BiS and Rotation

For endgame best in slot gear and rotation, head over to [https://www.akhmorning.com/jobs/blm/](https://www.akhmorning.com/jobs/blm/){target=_blank} or [https://discord.gg/thebalanceffxiv](https://discord.gg/thebalanceffxiv){target=_blank}
