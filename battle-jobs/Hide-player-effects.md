---
authors:
        - Robin Naite
tags:
    - Battle Effects
    - Spell Effects
---
# How can I hide player effects?

You can lower your Spell Effects in your settings menu under: Character Configuration -> Control Settings -> Character Tab.

![Battle Effects amount Settings](Images/Battle-effects.webp)

*Show Limited* will only show important effects such as the bubbles from {{WHM}} and {{SCH}}, or star from {{AST}}. Highly suggested to keep party to at least "Show Limited". Feel free to set Others to "Show None".
