---
authors:
        - Robin Naite
tags:
    - Job Unlock
---
# How to unlock a job at level 30

## Short Answer

You need to have done {{ MSQ }} level 20, called "Sylph Management" for the job advancement quests to appear.

So just continue doing your Main Story Quests!

You can easily see when it's available by looking at your Main Scenario tracker. The yellow **!** bellow the tracker indicates your job quests.

![MSQ Tracker](Images/msq_guide.webp)

Almost everything in this game is gated by the Main Scenario Quest.
