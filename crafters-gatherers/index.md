---
icon: material/grass
---

# Crafters and Gatherers

In this category you can find all information regarding FFXIV Crafting & Gathering, including how to unlock, questions, and general gameplay content.

## Commonly asked questions

*You can find more questions in the menu on the left on Desktop, or clicking the menu button on the top left on mobile.*

- [When do I need to start leveling crafters or gatherers?](When-do-I-need-to-start-leveling-Crafters-or-Gatherers.md)
