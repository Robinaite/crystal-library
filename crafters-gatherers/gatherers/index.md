---
authors:
        - Robin Naite
tags:
    - Gathering
---
# Gathering

Gathering Leveling Guide: [https://guides.ffxivteamcraft.com/guide/gathering-leveling-guide](https://guides.ffxivteamcraft.com/guide/gathering-leveling-guide)
Gathering Skills and Basics: [https://guides.ffxivteamcraft.com/guide/gathering-skills-and-basics](https://guides.ffxivteamcraft.com/guide/gathering-skills-and-basics)
Gathering Collectables Guide: [https://guides.ffxivteamcraft.com/guide/gathering-collectable-guide](https://guides.ffxivteamcraft.com/guide/gathering-collectable-guide)
Gathering Melding guide: [https://guides.ffxivteamcraft.com/guide/gathering-melding-guide](https://guides.ffxivteamcraft.com/guide/gathering-melding-guide)
