---
authors:
        - Robin Naite
tags:
    - Crafting
---
# Crafters

Crafting Melding Guide: [https://guides.ffxivteamcraft.com/guide/crafting-melding-guide](https://guides.ffxivteamcraft.com/guide/crafting-melding-guide)
Endgame Crafting Rotations: [https://guides.ffxivteamcraft.com/guide/endgame-crafting-rotations](https://guides.ffxivteamcraft.com/guide/endgame-crafting-rotations)
Expert Crafting Guide: [https://guides.ffxivteamcraft.com/guide/expert-crafting-guide](https://guides.ffxivteamcraft.com/guide/expert-crafting-guide)
