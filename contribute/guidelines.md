# Guidelines

## Summary

On this page you will find all the information regarding how the pages are structured and what you can do on each page.

## File names

The file names need to follow these requirements:

- No spaces in names (use - to indicate space)
- No special characters (- allowed)

## Page Layout

Each page needs to follow one of the templated layout (found [here](template-page.md). All templates are structured in the following format:

- Authors (obligatory)
- Tags/Meta data (obligatory)
- Title (Obligatory)
- Summary/Short Answer (Obligatory)
- More Content (Optional)

### Authors

Authors is used to indicate at the bottom of the page whom helped writing the page. Please set your name if you do so, and if you are updating a page, add it to the list.
### Tags

Tags are used for two purposes:

- Improve finding the pages
- usage in other tools such as discord bots (in the future)

The tags will always need to follow these rules:

- Main Tag (Obligatory)
- Secondary Tags (optional, up to 5)

Use the Tags page found [here](../tags.md), to see what tags already exist.

Make sure to remove any unused tags spaces.

#### Main Tags

Main tags are used for identifying the page and consists of a maximum of two words.
These tags need to be unique for each page.

#### Secondary Tags

Secondary tags are used to help finding the content on the pages. Try to use tags that already exist.

### Title

The title is used on two spots:

- As the page title (duh)
- As the navigation menu name

This allows for having different file names than title names.

### Summary / Short Answer

The summary or Short answer, is the first blob of information that will be seen by the user, and be shown in through the usage of the discord bot.

### More Content

Afterwards you are allowed to structure the rest of the page how you feel fit.
If you are using the Question & Answer template, make sure to use Long Answer as the secondary part, if necessary. Extra info, and See Also should always be after Long Answer

## Page content

Outside of everything available with markdown. There are various add-ons on this site that you can use to improve the pages.
You can read up more about how to use markdown here: [https://www.markdownguide.org/basic-syntax/](https://www.markdownguide.org/basic-syntax/){target=_blank}.
Discord makes use of basic markdown syntax as well, so doing things like bold text, or italic is the same!

### External URLs

If you are using external urls in your page, please add at the end:

```md
{target=_blank}
```

Example:

```md
[https://www.markdownguide.org/basic-syntax/](https://www.markdownguide.org/basic-syntax/){target=_blank}
```

This will make the links open in a new tab.

### Youtube Videos

To embed youtube videos you do the following:

```md
{/{ yt("videoID") }} (without the /)
```

You can find the youtube ID by looking at the url.

![YoutubeID](images/youtubeID.webp)

### Abbreviations

The website incorporates tooltips for abbreviations, that on hover shows the full explanation of a tooltip.

Example:

{{ arr }} when hovered will read "A Realm Reborn.

To do so, you need to do the following:

```md
{\{ arr }} (without the \ )
```

If you want to use abbreviations, check the [Glossary](../glossary/index.md) if it is already added.
If you want to add more abbreviations, pre-write it and report it on your merge request, or click here to know how to do it yourself.

### Info/Warning/Danger blocks

You can add Info, warning or danger blocks to your pages.

Example:

!!! warning "Title of this note"
    This is an example text!  
    The text should be one line indented.

How to write it:

```md
!!! warning "Title of this note"
    This is an example text!
    The text should be one line indented.
```

You can make them collapsible by changing the `!!!` to `???`.

Instead of warning you can write: info, success, danger
For a full list of blocks you can use check here: [Supported Types](https://squidfunk.github.io/mkdocs-material/reference/admonitions/#supported-types)
