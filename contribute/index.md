---
icon: material/exclamation
---

# How to contribute or report issues

You looked at this page because you want to help the project or report an issue? Thank you already!

- [How to report an issue](issue.md)
- [Edit or add content](contribute.md)
- [Guidelines on content](guidelines.md)
- [Template pages](template-page.md)
