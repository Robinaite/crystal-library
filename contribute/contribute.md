# How to Contribute

There are two ways to contribute to the project:

1. Create a Google Docs, and send them to Robinaites' on Twitter: [Robinaite Twitter](https://twitter.com/Robinaite){target=_blank}
2. Use Gitlabs Merge Requests system to add new or updated content to the project for review

You can find a link to all the content being worked on or questions that still need answering on the Issues board: [https://gitlab.com/Robinaite/crystal-library/-/boards](https://gitlab.com/Robinaite/crystal-library/-/boards){target=_blank}

## How do I use Gitlab merge request system?

To use Gitlab Merge Request system, you first need to Fork the project.

If you do not have the knowledge on how to use git, and do merge request I suggest you to use the second option and just report any issues or contribute to the project by sending over a google docs in Markdown format.

### Become a verified contributor

A verified contributor is someone who has helped fix various issues or added consistent quality questions and answers to the project.

A verified contributor has:

- Access to the project for direct editing of the files without needing to use forks. (making it thus easier)

??? note "Non-extensive information on how to use gitlab nonetheless"
    ### I want to fix an issue on a page or add more information to an existing page

    Pre-requisites:  

    - Having a [Gitlab](https://gitlab.com/){target=_blank} account.

    Follow the following steps:

    0. If you have done this before, check your own repository and make sure to delete it first, or look into mirroring.
    1. Go to the page with the issue.
    2. Click the edit button next to the title
    3. It will redirect you to gitlab
    4. Click on the edit button
        1. If its your first time editing the project, it will indicate "You can’t edit files directly in this project. Fork this project and submit a merge request with your changes." click Fork.
    5. Edit the texts with the issue. Please make sure you follow the guidelines found here: [Guidelines](guidelines.md)
    6. When finished, scroll down and update the Commit Message with a small description of what you changed.
    7. Click Commit changes
    8. It will load the page to make a new Merge Request.
        1. Update title and description if you want (not necessary)
    9. Scroll down and click "Create Merge Request"
    10. Now you wait for someone with access to the repository to review your changes and merge (update) them in the project. This can take up to 24 hours.

    *If you are knowledgeable with git, feel free to use git to create a merge request, you do not need to follow the steps above.*

    ### I want to add a new question or page to the website

    Pre-requisites:  

    - Having a [Gitlab](https://gitlab.com/) account.

    Follow the following steps:

    0. If you have done this before, check your own repository and make sure to delete it first, or look into mirroring.
    1. Go to the gitlab repository found here: [https://gitlab.com/Robinaite/ffxiv-faq](https://gitlab.com/Robinaite/ffxiv-faq)
    2. Find the "Web IDE" button and click it.
        1. If its your first time contributing to the project, a message will pop-up saying "You can’t edit files directly in this project. Fork this project and submit a merge request with your changes." Click Fork Project.
    3. On the left you will see the folder structure, hover over the folder you want to add a new file to.
    4. Click the 3 dots and the arrow downwards to show a dropdown
    5. Click "New File".
        1. If you have a file you want to upload yourself, click upload file.
        2. If you want to add a new directory, click "New Directory"
    6. Make sure to follow the guidelines on adding new files! You can find the guidelines here: [Guidelines](guidelines.md)
    7. When finished click Commit at the bottom.
    8. Write a commit message on what you added to the project.
    9. Select option "Create new branch"
    10. Have option "Start new merge request" enabled
    11. It will load the page to make a new Merge Request.
        1. Update title and description if you want (not necessary)
    12. Scroll down and click "Create Merge Request"
    13. Now you wait for someone with access to the repository to review your changes and merge (update) them in the project. This can take up to 24 hours.

    *If you are knowledgeable with git, feel free to use git to create a merge request, you do not need to follow the steps above.*
