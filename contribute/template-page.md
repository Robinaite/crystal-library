---
authors:
	- Robin Naite # Substitute my name
tags: # tags are to help categorize and search for specific stuff easier, as well as different ways to reference to this topic. For more check the guidelines document.
	- Main tag # this tag should be unique, consisting of the smallest amount of words possible.
	- secondary tag 1 (optional)
	- secondary tag 2 (optional)
	- secondary tag 3 (optional)
	- secondary tag 4 (optional)
	- Secondary tag 5 (optional)
---
# (Page title, write your page title here)

## Summary

Here you write a paragraph that answers the main question, or a small summary on what this page is about. This should be preferably smaller than 65 words. (Check guidelines as of why)

## main topic

Here you can write a longer answer or the full details about the topic.

## Extra Info (optional, make sure to delete these brackets before pushing)

Here you can write any extra information about the topic

## See Also (optional, make sure to delete these brackets before pushing)

Here you can indicate other websites or pages
