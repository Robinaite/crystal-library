---
authors:
    - Robin Naite # Substitute my name
tags: # tags are to help categorize and search for specific stuff easier, as well as different ways to reference to this topic. For more check the guidelines document.
    - Robin Naite
---
# Robin Naite (Project Maintainer)

## Introduction
Hey all! Best Lalafell here! I am the project maintainer for Crystal Library. I love helping out other people, and that was the main reason I started this project!

Tabletop games, Flute play, composing and programming are my go to things when I'm not helping people out!

## Links
:material-web: Website: [https://robinaite.com/](https://robinaite.com/)  
:material-twitter: Twitter: [https://twitter.com/robinaite](https://twitter.com/robinaite)

## Best Lala Picture
![](images/robinaite-screenshot.png)
