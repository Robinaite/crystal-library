---
description: FFXIV FAQ with quick search and speed! The place for all your random questions on Final Fantasy XIV! Quickly search for what you need!
authors: 
    - Robin Naite
hide:
    - navigation
    - toc
---

# FFXIV FAQ - Crystal Library 

## About

Welcome to the Crystal Library! A wonderful Frequently asked question place with answers to all (well almost) your questions regarding the game. Ranging from how to unlock other classes, tips & tricks, where to find the best guides, how do I buy the game, and much more!

Use the tabs above, or the search bar to search for your question.

## Categories

<div class="grid cards" markdown>

-   :money_with_wings:{ .lg .middle } __Game Purchases__

    ---

    Free Trial, Game Purchases, Editions, Subscription, Platforms

    [:octicons-arrow-right-24: See More](game-purchase/index.md)

-   :dagger:{ .lg .middle } __Battle Jobs__

    ---

    Unlocks, Materia & Melds, Leveling Guides, Macros, Game Settings

    [:octicons-arrow-right-24: See More](battle-jobs/index.md)

-   :world_map:{ .lg .middle } __Game Content__

    ---

    {{MSQ}}, Alliance Raids, Deep Dungeons, relic Weapons, Free Company, glamour, Poetics.

    [:octicons-arrow-right-24: See More](content/index.md)

-   :herb:{ .lg .middle } __Crafters & Gatherers__

    ---

    Guides, Unlocks, Leveling, questions and more!

    [:octicons-arrow-right-24: See More](crafters-gatherers/index.md)

-   :material-monitor:{ .lg .middle }   __User Interface__

    ---

    Gearsets, HUDs and settings!

    [:octicons-arrow-right-24: See More](user-interface/index.md)

-   :books:{ .lg .middle } __Glossary__

    ---

    Acronyms and definitions!

    [:octicons-arrow-right-24: See More](user-interface/index.md)

</div>