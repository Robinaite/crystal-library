---
authors:
        - Robin Naite
tags:
    - Versions
    - Steam
    - Windows
---
# What are the differences between the Steam and Windows (non-steam) versions?

## Short Answer

The Steam and Windows versions are the same game, only differing in payment options, and regions availability. The versions are cross-play with each other

Steam allows the user to pay for their subscription, or cash shop items using the steam wallet.

It's also made available in more regions than the Windows version.

!!! danger "Careful"
    Make sure that if you buy into one or the other, you will always need to buy any expansion in the future on the respective store.
