---
authors:
        - Robin Naite
tags:
    - Buy Windows
    - Upgrade Windows
---
# How do I buy the game on Windows (non-steam)?

## Requirements

You need to be on the Windows Free Trial. You can check where your free trial is in [How do I create a Free Trial Account?](../free-trial-account/index.md)

If you are on the ***STEAM*, MAC or Playstation Free trial**, please look here for the respective guides: [How to Buy the game](index.md)

## Steps to buy

1. Go to your regions' Square Enix Store.
2. Buy the Complete Edition.
3. Wait on the email, or check Bought Products page for the Code
4. Now go to the Mogstation here: [https://secure.square-enix.com/account/app/svc/mogstation/](https://secure.square-enix.com/account/app/svc/mogstation/){target=_blank}
5. Login with the same account you use to login into the game
6. Click "Transfer to Regular Service"
10. Input the code you saw on Step 3.
11. Continue following the steps and accept the terms.
12. Congrats, now launch the game, login, and let it download the missing expansions!

## Store Links

SE Store EU: [https://store.eu.square-enix-games.com/en_EU/games/franchises/final-fantasy-xiv-online](https://store.eu.square-enix-games.com/en_EU/games/franchises/final-fantasy-xiv-online){target=_blank}

SE Store France: [https://store.eu.square-enix-games.com/fr_FR/games/franchises/final-fantasy-xiv-online](https://store.eu.square-enix-games.com/fr_FR/games/franchises/final-fantasy-xiv-online){target=_blank}

SE Store Germany: [https://store.eu.square-enix-games.com/de_DE/games/franchises/final-fantasy-xiv-online](https://store.eu.square-enix-games.com/de_DE/games/franchises/final-fantasy-xiv-online){target=_blank}

SE Store UK: [https://store.eu.square-enix-games.com/en_GB/games/franchises/final-fantasy-xiv-online](https://store.eu.square-enix-games.com/en_GB/games/franchises/final-fantasy-xiv-online){target=_blank}

SE Store US: [https://store.na.square-enix-games.com/en_US/search/Final%20Fantasy%20XIV](https://store.na.square-enix-games.com/en_US/search/Final%20Fantasy%20XIV){target=_blank}

SE Store Australia/New Zealand: [https://store.eu.square-enix-games.com/en_AU/search/Final%20Fantasy%20XIV](https://store.eu.square-enix-games.com/en_AU/search/Final%20Fantasy%20XIV){target=_blank}
