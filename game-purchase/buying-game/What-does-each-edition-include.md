---
authors:
        - Robin Naite
tags:
    - Editions
    - Complete Edition
    - Starter Edition
    - Endwalker
    - Shadowbringers
---
# What does each edition include?

### Starter Edition

(basically lifts the free trial restrictions, but no extra content)

- A Realm Reborn & Heavensward Expansion
- 30 days of "free" sub time

### Shadowbringers

- Includes Stormblood Expansion

### Pre-order Endwalker

- Will include Stormblood and Shadowbringers, but only on release (19 November 2021)

### Complete Edition 

(before release of Endwalker. does not include Endwalker yet)

- Starter Edition
- Shadowbringers

## Extra Info

When Endwalker releases you will not be able to buy Shadowbringers. Which will be substituted by Endwalker, which will include SB and ShB. The complete Edition will then be a bundle of Starter Edition + Endwalker.
