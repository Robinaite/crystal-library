---
authors:
        - Reiko
tags: 
- new character
- character creation
- creation restrictions
- server status
---
# The server says it's full, how can I create a character on it?

## Short Answer

First, check the server status [here](https://na.finalfantasyxiv.com/lodestone/worldstatus/){target=_blank}. Unless it is listed as "Congested" which is quite rare these days, then the world is not actually full. Instead, it is temporarily restricting the creation of new characters, indicated on the server status page with a red figure with an 'x'. This restriction is based on current logged in population. So if you wish to create a character on that world, simply wait until it becomes a less popular time to play. This is usually late at night, early morning or possibly during the work day.
