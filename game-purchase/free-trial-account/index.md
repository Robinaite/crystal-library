---
authors:
        - President Tag
        - Robin Naite
tags:
    - FT account
    - Free Trial Account
    - Account Issues
---
# How do I create a Free Trial account?

## Short Answer

The process of getting a Free Trial Account is terrible, thus read the Longer Answer for a Step by Step Process with Screenshots to get you through it without issues.

## Longer Answer

### Preparations

If you do not have a Square Enix account then you can scroll down to the respective platform.

If you have already have a Square Enix Account or a Free Trial Account do the following:

1. Login into the Mogstation: [https://secure.square-enix.com/account/app/svc/mogstation/](https://secure.square-enix.com/account/app/svc/mogstation/){target=_blank}
2. When logged in, check what it shows on the next screen.

#### Add New Service Account

If it Says Add a new Service account, you can:

1. Choose Steam, Windows, PlayStation or Mac platforms to play your trial on. Select your platform further down for more information.

### Account Type: Free Trial

![Free Trial Non Steam](Images/FreeTrialAccount-and-upgrade/FreeTrialNonSteam.webp)
If it just Says Free Trial, like in the screenshot above, you can:

1. Only play the trial from the Final Fantasy XIV Free Trial Website, and ***NOT*** from Steam!

### Account Type: Free Trial (Steam)

![Free Trial Steam](Images/FreeTrialAccount-and-upgrade/FreeTrialSteam.webp)
If the account type mentions Free Trial with (steam) behind it, like the image above you NEED to:

1. Get the Free Trial on Steam ***ONLY***!

### Platforms

Select the platform below where you want to start the Free Trial. If you already have an account make sure to check the preparation steps to know which platform to choose.

- [Windows](Windows.md)
- [Steam](Steam.md)
- [Mac](Mac.md)
- [PlayStation](PlayStation.md)

## Useful Links

### Launcher Download Links

Windows (NOT STEAM) Launcher download: [https://freetrial.finalfantasyxiv.com/download/?platform=win&lng=en-gb&rgn=eu](https://freetrial.finalfantasyxiv.com/download/?platform=win&lng=en-gb&rgn=eu){target=_blank}

Mac Launcher Download page: [https://freetrial.finalfantasyxiv.com/download/?platform=mac&lng=en-gb&rgn=eu](https://freetrial.finalfantasyxiv.com/download/?platform=mac&lng=en-gb&rgn=eu){target=_blank}

### Account Management & Subscription Payment

Mogstation: [https://secure.square-enix.com/account/app/svc/mogstation/](https://secure.square-enix.com/account/app/svc/mogstation/){target=_blank}

### Store Links

Steam Free Trial Page: [https://store.steampowered.com/app/312060/FINAL_FANTASY_XIV_Online_Free_Trial/](https://store.steampowered.com/app/312060/FINAL_FANTASY_XIV_Online_Free_Trial/){target=_blank}
Steam Main Page: [https://store.steampowered.com/app/39210/FINAL_FANTASY_XIV_Online/](https://store.steampowered.com/app/39210/FINAL_FANTASY_XIV_Online/){target=_blank}

SE Store EU: [https://store.eu.square-enix-games.com/en_EU/games/franchises/final-fantasy-xiv-online](https://store.eu.square-enix-games.com/en_EU/games/franchises/final-fantasy-xiv-online){target=_blank}

SE Store France: [https://store.eu.square-enix-games.com/fr_FR/games/franchises/final-fantasy-xiv-online](https://store.eu.square-enix-games.com/fr_FR/games/franchises/final-fantasy-xiv-online){target=_blank}

SE Store Germany: [https://store.eu.square-enix-games.com/de_DE/games/franchises/final-fantasy-xiv-online](https://store.eu.square-enix-games.com/de_DE/games/franchises/final-fantasy-xiv-online){target=_blank}

SE Store UK: [https://store.eu.square-enix-games.com/en_GB/games/franchises/final-fantasy-xiv-online](https://store.eu.square-enix-games.com/en_GB/games/franchises/final-fantasy-xiv-online){target=_blank}

SE Store US: [https://store.na.square-enix-games.com/en_US/search/Final%20Fantasy%20XIV](https://store.na.square-enix-games.com/en_US/search/Final%20Fantasy%20XIV){target=_blank}

SE Store Australia/New Zealand: [https://store.eu.square-enix-games.com/en_AU/search/Final%20Fantasy%20XIV](https://store.eu.square-enix-games.com/en_AU/search/Final%20Fantasy%20XIV){target=_blank}

## Issues & Solutions

### Error Code i2501

This is a nasty error as it could be tied to multiple things, it can be caused by an abnormal connection, a public network (Internet cafe, University connection), the use of a VPN or simply too many rapid attempts. The system will try to filter out anything it finds to be "strange."

Try the following options when creating an account:

- Use another browser
- Turn off VPN if being used
- Disable Adblock
- Restart Router
- Swap to wifi or ethernet (opposite)
- Create Account on your Smartphone using Mobile Data
- Try again the next day

### Free Trial Registration Code

If in the launcher it is asking for a registration code follow these steps:

1. Close the Launcher
2. Open the launcher again
3. Repeat the process

If it still doesn't work, check the issue below

### I Already have a FFXIV account, but I cant get through the setup page

Go to My Documents/MyGames/Final fantasy XIV directory, open FFXIV_BOOT.cfg and change StartupCompleted from 0 to 1.
It will now bypass the setup screen and show you the login page.
