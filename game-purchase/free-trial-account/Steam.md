---
authors:
        - President Tag
        - Robin Naite
tags:
    - Steam Free Trial
    - Free Trial
---
# How do I create a Free Trial account on Steam?

## Preparations

If you do not have a Square Enix account then you can continue with creating the account.

If you have already have a Square Enix Account or a Free Trial Account make sure to check Prepararations in: [How to create a free trial account](index.md)

## Creating the Account

*If you already have an account, you can skip steps 5-12*

1. Install the Free Trial through Steam (if it does not appear, check Issues at the end)
    1. Free Trial Page
        1. Search for "FFXIV Free Trial", and click on it.
        2. Scroll down, and click "Install Demo"
        ![Install Demo FFXIV Trial Page](Images\FreeTrialAccount-and-upgrade\Steam\Steam-Free-Trial-Install-Demo.webp)         
    2. Full Page
        1. Search for "Final Fantasy XIV Online"
        2. Scroll down, and click "Download Demo"  
        ![Install Demo FFXIV Page](Images\FreeTrialAccount-and-upgrade\Steam\Steam-FFXIV-Page-Download_Demo.webp)    
2. When finished open the launcher.
3. Accept the Terms of the License Agreement.
![License Agreement](Images/FreeTrialAccount-and-upgrade/Steam/Launcher-Steps/1-Launcher-TermsOfLicenseAccept.webp)
4. Click on Next
![Steam Next Code 1](Images/FreeTrialAccount-and-upgrade/Steam/Launcher-Steps/2-Launcher-StartScreen.webp)
5. Select the option that qualifies for you.
![Steam Select Account Creation or Login](Images/FreeTrialAccount-and-upgrade/Steam/Launcher-Steps/3-Launcher-AccountCreationOrLogin.webp)
6. Select the country you live in.
![Steam New Account Select Country](Images/FreeTrialAccount-and-upgrade/Steam/Launcher-Steps/4-Launcher-NewAccount-SelectCountry.webp)
7. Read, Accept the Licenses and click Next.
![Steam New Account More licenses](Images/FreeTrialAccount-and-upgrade/Steam/Launcher-Steps/5-Launcher-NewAccount-LicensesAccept.webp)
8. Create your SE account (Make sure to remember Security Question & Answer as its often used in recovering passwords!)
![Steam New Account Details](Images/FreeTrialAccount-and-upgrade/Steam/Launcher-Steps/6-Launcher-NewAccount-Details.webp)
9. Double-check your email that it is all lower case letters.
10. Check your email for a confirmation code
![Steam new Account confirmation Code Email](Images/FreeTrialAccount-and-upgrade/Steam/Launcher-Steps/8-NewAccount-ConfirmationCodeEmail.webp)
11. Enter the confirmation code inside your launcher
![Steam New Account Confirmation Code Input](Images/FreeTrialAccount-and-upgrade/Steam/Launcher-Steps/7-Launcher-New-Account-ConfirmationCodeInput.webp)
12. You will get an email confirming that the account was created, click Next on the launcher
![Steam New Account Successfully created](Images/FreeTrialAccount-and-upgrade/Steam/Launcher-Steps/9-Launcher-AccountCreatedScreen.webp)
13. **!!!** If you SEE a "Add Registration Code" page do NOT panic (like the image below! Continue following the steps.
![Steam Account Registration Code](Images/FreeTrialAccount-and-upgrade/Steam/Launcher-Steps/10-Launcher-RegisterCode.webp)
14. Restart launcher and check if the screen shown now indicates the Free trial(check image below).
![Steam Free Trial Startup Page](Images/FreeTrialAccount-and-upgrade/Steam/Launcher-Steps/11-Launcher-StartUpScreenFreeTrial-correct.webp)
15. If it shows, you're good to go! Click Next!
16. Press: "I Currently possess a Square Enix Account"
![Steam Account Creation or Login Page 2](Images/FreeTrialAccount-and-upgrade/Steam/Launcher-Steps/12-Launcher-AccountCreationOrLogin-2ndTry.webp)
17. Login with the account you just created, **and leave "One-time Password" empty**!
![Steam Account Login Page](Images/FreeTrialAccount-and-upgrade/Steam/Launcher-Steps/13-Launcher-Login.webp)
18. Accept the Free Trial Terms.
![Steam Account Free Trial Terms](Images/FreeTrialAccount-and-upgrade/Steam/Launcher-Steps/14-Launcher-FreeTrialTerms.webp)
19. Press Next and See the confirmation window
![Steam Account Download Game](Images/FreeTrialAccount-and-upgrade/Steam/Launcher-Steps/15-Launcher-RegistrationComplete.webp)
20. Press Next and let the launcher download the Free Trial!
![Steam Account Download Game](Images/FreeTrialAccount-and-upgrade/Steam/Launcher-Steps/16-Launcher-Downloading-Game.webp)

## Useful Links

### Store Links

Steam Free Trial Page: [https://store.steampowered.com/app/312060/FINAL_FANTASY_XIV_Online_Free_Trial/](https://store.steampowered.com/app/312060/FINAL_FANTASY_XIV_Online_Free_Trial/){target=_blank}
Steam Main Page: [https://store.steampowered.com/app/39210/FINAL_FANTASY_XIV_Online/](https://store.steampowered.com/app/39210/FINAL_FANTASY_XIV_Online/){target=_blank}
