---
authors:
        - President Tag
        - Robin Naite
tags:
    - Mac free trial
    - Free Trial
---
# How do I create a Free Trial account on a Mac?

## Preparations

If you do not have a Square Enix account then you can continue with creating the account.

If you have already have a Square Enix Account or a Free Trial Account make sure to check Prepararations in: [How to create a free trial account](index.md)

## Creating Free Trial Account

*If you already have an account, you can skip to step 4, check issues further down for download link*

1. Go to: [https://freetrial.finalfantasyxiv.com/](https://freetrial.finalfantasyxiv.com/){target=_blank}
2. Double check your region is correct at the bottom of the page.
3. Click on "Sign Up Now"
    1. No Account
        1. Fill in the necessary information
        ![new Account Register](Images\FreeTrialAccount-and-upgrade\FreeTrial-Windows-Mac\NewAccount\1-FreeTrial-NewAccountRegister.webp)
        1. Make sure to select the correct platform where you want to play the trial, and click Submit
        ![Free Trial platform choice](Images\FreeTrialAccount-and-upgrade\FreeTrial-Windows-Mac\NewAccount\2-FreeTrial-PlatformChoice.webp)
        1. Go to your Email to get the Confirmation Code
        ![Free Trial confirmation email](Images/FreeTrialAccount-and-upgrade/FreeTrial-Windows-Mac/NewAccount/4-NewAccount-ConfirmationCodeEmail.webp)
        1. Input the confirmation code on the website and click submit
        ![Free Trial confirmation submit](Images/FreeTrialAccount-and-upgrade/FreeTrial-Windows-Mac/NewAccount/3-FreeTrial-NewAccount-CreationCode.webp)
        1. Account successfully created, click download to download the launcher
        ![Free Trial account created](Images/FreeTrialAccount-and-upgrade/FreeTrial-Windows-Mac/NewAccount/5-FreeTrial-SuccesfullAccountCreated.webp)
    2. Square Enix Account
        *Use this process if you already have a Square Enix account (NOT SQUARE ENIX STORE!) but never played FFXIV, if in doubt check preparation above*

        1. Click the Sign In SE Account Button  
            ![Login SE Account Button](Images\FreeTrialAccount-and-upgrade\FreeTrial-Windows-Mac\SEAccount\1-FreeTrial-SignInSEAccount.webp)
        2. Login with your Square Enix Account
        ![Login SE Account Page](Images\FreeTrialAccount-and-upgrade\FreeTrial-Windows-Mac\SEAccount\2-FreeTrial-SEAccountLogin.webp)
        1. Select the correct platform where you want to play the trial
        ![Select Platform](Images/FreeTrialAccount-and-upgrade/FreeTrial-Windows-Mac/SEAccount/3-FreeTrial-SEAccount-PlatformChoice1.webp)
        1. Account successfully created, click download to download the launcher
        ![Free Trial account created](Images/FreeTrialAccount-and-upgrade/FreeTrial-Windows-Mac/SEAccount/5-FreeTrial-SuccesfullAccountCreated.webp)

4. Click Download Game Button
5. Follow the On Screen instruction on how to install FFXIV
6. Open the launcher (Below steps are the same as windows, no access to a Mac installation to get correct screenshots yet.)
7. Accept the Terms of License
![Accept Terms of License](Images/FreeTrialAccount-and-upgrade/FreeTrial-Windows-Mac/Launcher-Steps/1-Launcher-TermsOfLicenseAccept.webp)
8. Click next on the Startup Screen
![Startup Screen](Images/FreeTrialAccount-and-upgrade/FreeTrial-Windows-Mac/Launcher-Steps/2-Launcher-StartUpScreenFreeTrial-correct.webp)
9. Click "I currently possess a Square Enix Account"
![Creation or Login](Images/FreeTrialAccount-and-upgrade/FreeTrial-Windows-Mac/Launcher-Steps/3-Launcher-AccountCreationOrLogin.webp)
10. Login with your Square Enix Account
![Launcher Login](Images/FreeTrialAccount-and-upgrade/FreeTrial-Windows-Mac/Launcher-Steps/4-Launcher-Login.webp)
11. click Next and let the Launcher Download the game!
![Launcher Download Game](Images/FreeTrialAccount-and-upgrade/FreeTrial-Windows-Mac/Launcher-Steps/6-Launcher-Downloading-Game.webp)
12. If the game asks for Registration code, dont panic, CLOSE the launcher, and open it again, and it should work. If still not, check Issues below
![Launcher Download Game](Images\FreeTrialAccount-and-upgrade\FreeTrial-Windows-Mac\Launcher-Steps\Potentially-Launcher-RegisterCode.webp)
