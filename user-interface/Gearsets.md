---
authors:
        - Robin Naite
tags:
    - Gearsets
    - UI
---
# Gearsets

## What are Gearsets?

Gearsets are an amazing tool which allow you to save a whole set of items you have equipped for easy swapping between them!
Its often used to easily swap between jobs and create those fancy change jobs hotbars.

## How do I create a Gearset?

1. Open your Character Profile.
2. Click on the gearset icon. The icon is above your weapon, the one on the left that looks like on paper.
![Open Gear Set window button](Images/2_gearset_button.webp)
3. Click on the + sign to create a new gear set if you haven't created one yet
![New Gear Set Button](Images/3_gearset_new_button.webp)

## How do I keep the Gearset Updated?

You can easily keep the gearset updated by clicking on the sync button on your character profile after equipping new gear. This will update the current gearset shown to the new equipment!

![Update Gear Set Button](Images/update_gearset_button.webp)

## How do I swap between gearsets?

You can swap between gearsets by:

- Opening the gearset window and selecting/double clicking on the gearset
- writing /gs change "gs" number in the chat, substituting "gs" with the number
- Clicking a gearset button. You can find how to create a gearset (also known as job change) button here: [How do I setup job change buttons](Job-change-buttons.md)
