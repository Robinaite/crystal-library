---
icon: material/monitor
authors:
        - Robin Naite
tags:
    - UI
---
# User Interface

Have questions regarding User Interface in the game? Look here!

I highly suggest watching the following video from Zepla on UI:
{{yt("R0buenOiW5Q")}}

## Topics:

{% for subPage in page.parent.children %}
{% if subPage.file.url.split('/')[1]|length %}
 - [{{ subPage.file.url.split('/')[1].capitalize()|replace('-',' ') }}](../{{subPage.url}})
{% endif %}
{% endfor %}