---
authors:
        - Robin Naite
tags:
    - Job Change Button
    - Gearsets
    - UI
---
# Job Change Buttons

To have job change buttons you need to make use of gearsets. You can find how to make gearsets here: [Gearsets](Gearsets.md).

Now drag and drop the gearset you want with the mouse (or virtual mouse) onto a hotbar like the gif below.

![New Gear Set Button](Images/4_gearset_drag_and_drop.gif)

*Make sure to keep the gearsets updated by using the refresh button on the top right in your character profile.*

## Macro buttons or text commands

You can also setup a job change button by using macro buttons or text command.

You just need to add the text (by substituting "gearset number", including the ", with the number assigned to it in the window):

*/gearset change "gearset number"*
