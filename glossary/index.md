---
icon: material/bookshelf
hide:
    - navigation
search:
    boost: -100
---
# Glossary

Scroll down to find the full description of an Abbreviation

## Expansions

| Abbreviation      | Description                          |
| ---------------- | ------------------------------------ |
| ARR | A Realm Reborn |
| HW | Heavensward |
| SB | Stormblood |
| ShB | Shadowbringers |
| EW | Endwalker |

## Questing

| Abbreviation      | Description                          |
| ---------------- | ------------------------------------ |
| MSQ | Main Scenario Quests |

## Game

| Abbreviation      | Description                          |
| ---------------- | ------------------------------------ |
| FC | Free Company (Player Guilds) |
| POTD | Palace of the Dead |
| HOH | Heaven on High |

## Battle

| Abbreviation      | Description                          |
| ---------------- | ------------------------------------ |
| oGCD | Off Global Cooldown |
| GCD | Global Cooldown |

## Jobs

### DPS

| Abbreviation      | Description                          |
| ---------------- | ------------------------------------ |
| BLM | Black Mage |
| SMN | Summoner |
| RDM | Red Mage |
| BRD | Bard |
| MCH | Machinist |
| DNC | Dancer |
| MNK | Monk |
| DRG | Dragoon |
| NIN | Ninja |
| SAM | Samurai |
| RPR | Reaper |

### Tanks

| Abbreviation      | Description                          |
| ---------------- | ------------------------------------ |
| PLD | Paladin |
| WAR | Warrior |
| DRK | Dark Knight |
| GNB | Gunbreaker |

### Healers

| Abbreviation      | Description                          |
| ---------------- | ------------------------------------ |
| WHM | White Mage |
| SCH | Scholar |
| AST | Astrologian |
| SGE | Sage |

## Class

### DPS

| Abbreviation      | Description                          |
| ---------------- | ------------------------------------ |
| THM | Thaumaturge |
| ACN | Arcanist |
| ARC | Archer |
| PGL | Pugilist |
| LNC | Lancer |
| RGE | Rogue |

### Tanks

| Abbreviation      | Description                          |
| ---------------- | ------------------------------------ |
| GLD | Gladiator |
| MAR | Marauder |

### Healers

| Abbreviation      | Description                          |
| ---------------- | ------------------------------------ |
| CNJ | Conjurer |

## Gatherers

| Abbreviation      | Description                          |
| ---------------- | ------------------------------------ |
| BTN | Botanist |
| MIN | Miner |
| FSH | Fisher |

## Crafters

| Abbreviation      | Description                          |
| ---------------- | ------------------------------------ |
| ALC | Alchemist |
| ARM | Armorer |
| BSM | Blacksmith |
| CRP | Carpenter |
| CUL | Culinarian |
| GSM | Goldsmith |
| LTW | Leatherworker |
| WVR | Weaver |
