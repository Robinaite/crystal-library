---
authors:
        - Robin Naite
tags:
    - Make Gil
---
# How do I make gil?

## Short Answer

There are various methods to make gil:

- Crafting & Gathering
- Retainer Ventures
- Treasure Maps
- Extreme Trials farming
- Hunts
- Completing Quests

How to Make Gil without Crafting or Gathering in FFXIV from ZeplaHQ:
{{yt("nYjbaoTs318")}}
