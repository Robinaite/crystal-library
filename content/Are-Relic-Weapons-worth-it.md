---
authors:
        - Robin Naite
tags:
    - Relics
    - Relic Weapons
---
# Are Relic Weapons worth it?

## Short Answer

For better stats, outside of the current expansion relic, no. For visuals, they look amazing. Be prepared for some grind though!

## Long Answer

Each expansion has their own relic quest line. These quest lines take hours upon hours to finish to the end, thus requiring a lot of grinding (like a lot, really a lot). The relics weapons are at most a bit better than the best weapon you can buy with tomestones. For any of the older expansions, its not worth stopping MSQ progress just for the slight boost in stats.
They do look really cool though, so if you want some cool looking weapons be prepared for some grinding! Below you can find links to guides for each expansion on how to get the relics and a demonstration on how they look.

### Relic Weapon Demonstrations

Shadowbringer Relic Weapons:
{{ yt("gOAUemiJ2Ic") }}

Stormblood Relic Weapons:
{{ yt("ddSph13LecE") }}

Heavensward Relic Weapons:
{{ yt("AfbQMMVfFu4") }}

A Realm Reborn Relic Weapons:
{{ yt("7pV83gom4XY") }}

### Relic Weapon Guides

Shadowbringer Relic Weapons:

- [https://www.akhmorning.com/resources/bozjan-southern-front/relic-weapons/#shadowbringers-resistance-relic-weapons](https://www.akhmorning.com/resources/bozjan-southern-front/relic-weapons/#shadowbringers-resistance-relic-weapons){target=_blank}
- [https://ffxiv.gamerescape.com/wiki/Category:Resistance_Weapons](https://ffxiv.gamerescape.com/wiki/Category:Resistance_Weapons){target=_blank}

Stormblood Relic Weapons:

- [https://ffxiv.gamerescape.com/wiki/Category:Eureka_Weapons](https://ffxiv.gamerescape.com/wiki/Category:Eureka_Weapons){target=_blank}

Heavensward Relic Weapons:

- [https://ffxiv.gamerescape.com/wiki/Category:Anima_Weapons](https://ffxiv.gamerescape.com/wiki/Category:Anima_Weapons){target=_blank}

A Realm Reborn Relic Weapons:

- [https://ffxiv.gamerescape.com/wiki/Category:Zodiac_Weapons](https://ffxiv.gamerescape.com/wiki/Category:Zodiac_Weapons){target=_blank}
