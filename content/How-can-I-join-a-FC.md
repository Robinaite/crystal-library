---
authors:
        - Robin Naite
tags:
    - FC
    - Social
---
# How can I join a Free Company?

## Short Answer

Free Companies are FFXIV version of Player Guilds. These are mostly social groups.

Joining a {{FC}} is worth it, as it can give you access to {{FC}} buffs, and groups of people to play with!

Do not be afraid to leave the {{FC}} if its not to your taste!

To join a {{FC}} you can apply to one through their {{FC}} menu, or by getting invited to one. To get to the {{FC}} menu you either need to go to their House if they have any, or player search/examine, one of the {{FC}} members to get to this menu.

You can find {{FC}}s and how to join them on the Community Finder website on Lodestone. [https://eu.finalfantasyxiv.com/lodestone/community_finder/](https://eu.finalfantasyxiv.com/lodestone/community_finder/){target=_blank}

Make sure to select your world, as {{FC}}s are Home world only.
