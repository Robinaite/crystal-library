---
authors:
        - Robin Naite
tags:
    - Glamour
---
# How do I apply glamour?

## Short Answer

First you need to unlock it, by doing the quest "If I had Glamour" in Vesper Bay.

After you have it unlocked you only need to right click on one of your equipments and then "Cast Glamour".

This will show a list of all the items that you can use as glamour.

Glamour is a complex system with various features in FFXIV. For a quick rundown I suggest watching the following video from ZeplaHQ:

{{yt("QQxIuKtfrAk")}}

## Long Answer

### How to unlock Glamour

To unlock glamour you need to do the quest. [If I had Glamour](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/bb7ebd31069/){target=_blank}, in Vesper Bay.

### Glamour Requirements

The glamour system always uses the following rules:

- You need to have Glamour Prisms
- The equipped item needs to be of higher Level (and then Item Level) than the item you want to use as glam.
- You need to be able to equip the gear to be able to use it as glam (so no cross-job glamours)

### How to remove glamour from an item

You can remove glamour from an item by:

1. Having Glamour Dispellers
2. Right clicking your gear
3. Clicking Cast Glamour
4. Clicking "none"

### Where can I get Glamour Prisms and Glamour Dispellers?

The best way to get glamour prisms and glamour dispellers is from your Grand Company using seals.

You can also get them form the marketboard, or from PvP Wolf Marks.

### How do I use glamour plates

Glamour plates are preset glamours that you can apply with one button on your whole gear.

To create glamour plates you need to:

1. Go to an Inn
2. Click on the glamour dresser
3. Add the items you want to the glamour dresser
4. Click on glamour plates
5. Setup one plate
6. Click Save

To use a glamour plate:

1. Open you character profile
2. Select the glamour plates option
3. Hit Apply

You can only apply glamour plate in big cities.

!!! info "Glamour plates to gearsets"
    Did you know you can link glamour plates to specific gearsets? Well now you know! This means that when using that specific gearset, the glamour plate will automatically apply as well, if possible.
