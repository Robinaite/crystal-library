---
authors:
        - Robin Naite
tags:
    - Hunts
    - Elite Marks
---
# How do I unlock Hunts or Elite Marks?

## What are Hunts and Elite Marks?

Hunts are bounties you can pickup from various bountie boards to kill certain mobs for special currencies.
Elite marks are roaming monsters that are ranked from Rank B, to Rank S for difficulty and spawning rate. 

Various communities organise hunt trains to help kill Elite marks for good xp, tomestones and hunts currency.

## What are Hunt trains?

Hunt trains are community organized routes that will setup routes to kill various Rank A Elite marks in a row on a whole world. These routes are scouted prior.

For more information to know when hunt trains are happening join one of the following discords:

- Centurio Hunts: [https://discord.com/invite/dZTgnpv](https://discord.com/invite/dZTgnpv)
- Faloop Hunts: [https://faloop.app/community](https://faloop.app/community)

## Where are these monsters?

The bills are found in the special items section. When clicked there is an option to show the general area on the map.

You can easily identify them by seeing a small red icon above their names.

You can also access one of the following sites to have more information on where they are:

- Faloop: [https://faloop.app/](https://faloop.app/)
- FFXIV Hunt: [https://ffxivhunt.com/](https://ffxivhunt.com/) 

## How to unlock Hunts?

To unlock the bounties for hunts you need to unlock them by doing the following quests in order.

*You need to do all the quests in ARR to see the HW, and the ones in HW to see the SB, etc...*

### A Realm Reborn

A realm reborn hunts require you to be at least **Second Lieutenant** in your Grand Company!

**Maelstrom**: [Let The Hunt Begin (Maelstrom)](https://eu.finalfantasyxiv.com/lodestone/playguide/db/quest/363811b27b4/)  
**Twin Adder**: [Let The Hunt Begin (Twin Adder)](https://eu.finalfantasyxiv.com/lodestone/playguide/db/quest/32c7488b0f1/)  
**Immortal Flames**: [Let The Hunt Begin (Immortal Flames)](https://eu.finalfantasyxiv.com/lodestone/playguide/db/quest/0262fe02207/)  

### Heavensward

1. Level 53 (Foundation): [Let the Clan Hunt Begin](https://eu.finalfantasyxiv.com/lodestone/playguide/db/quest/78b452d40d8/)
2. Level 56 (Foundation): [Better Bill Hunting](https://eu.finalfantasyxiv.com/lodestone/playguide/db/quest/d2a0497a5cb/)
3. Level 59 (Idyllshire): [Top Marks](https://eu.finalfantasyxiv.com/lodestone/playguide/db/quest/04edf80cf2a/)
4. Level 60 (Foundation): [Elite and Dangerous](https://eu.finalfantasyxiv.com/lodestone/playguide/db/quest/a5faad05086/)

### Storbmlood

1. Level 61 (Kugane): [One-star Veteran Clan Hunt](https://eu.finalfantasyxiv.com/lodestone/playguide/db/quest/f0a91b02e0b/)
2. Level 63 (Kugane): [Two-star Veteran Clan Hunt](https://eu.finalfantasyxiv.com/lodestone/playguide/db/quest/eba740d39bb/)
3. Level 66 (Kugane): [Three-star Veteran Clan Hunt](https://eu.finalfantasyxiv.com/lodestone/playguide/db/quest/eba740d39bb/)
4. Level 70 (Kugane): [Elite Veteran Clan Hunt](https://eu.finalfantasyxiv.com/lodestone/playguide/db/quest/f704d1dfcc8/)

### Shadowbringers

1. Level 70 (Crystarium): [Nuts to you](https://eu.finalfantasyxiv.com/lodestone/playguide/db/quest/f2816833d78/)
2. Level 73 (Crystarium): [Two Nuts Too Nutty ](https://eu.finalfantasyxiv.com/lodestone/playguide/db/quest/293910d3727/)
3. Level 76 (Crystarium): [How Do You Like Three Nuts](https://eu.finalfantasyxiv.com/lodestone/playguide/db/quest/1b95adabe01/)
4. Level 80 (Crystarium): [Too Many Nutters](https://eu.finalfantasyxiv.com/lodestone/playguide/db/quest/cede22e065d/)

### Endwalker

1. Level 80 (Old Sharlayan): The Hunt for Specimens 
2. Level 83 (Old Sharlayan): That Specimen Came from the Moon
3. Level 86 (Old Sharlayan): A Hunt for the Ages
4. Level 90 (Old Sharlayan): Perfect Specimens

## FAQ

### Why am I not receiving rewards for killing Elite marks?

To receive rewards from Elite Marks you need to have done the quests.
