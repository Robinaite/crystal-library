---
authors:
    - Reiko
tags: 
- Savage Raids
- Loot
- Gearing
---
# How does loot work for Savage raids?

## Short Answer

Loot for a current tier savage raid works a bit differently and is more restrictive than other content. You only get a single *chance* per week per raid to get anything. Also, you must reclear each fight in order, if you start at a later raid, you automatically forfeit that week's chance at loot for any raids before that one. Finally, the amount of chests that drop is dependent on how many party members have already cleared that raid for that week.

- 0 cleared = 2 chests,
- 1-4 have cleared = 1 chest,
- 5-8 have cleared = 0 chests

## Longer Answer

What type of loot that drops is set by each fight and has a fixed pattern that is contant for each tier (Note: Drops listed are assuming a 2 chest run):

- 1st Raid: 3 gear coffers for any of the accessories
- 2nd Raid: 2 Head, Hands or Feet gear coffers; 1 item used in augmenting current tome gear accessories; 1 token used in obtaining a current tome weapon
- 3rd Raid: 1 Head, Hands, or Feet gear coffer; 1 Legs gear coffer, 1 tome augmentation item for armor; 1 tome augmentation item for a weapon.
- 4th Raid: 1 Chest gear coffer; 1 weapon coffer; 1 actual weapon; 1 mount; 1 minion (same one as normal mode)

## Extra Info

Keep in mind also that, although you only get a single chance at the loot each week, unlike other current tier loot restricted content like the normal difficulty raids and the alliance raid, you *can* obtain more than one drop if you simply have lucky rolls. Also, even if you do not get anything, you will always recieve a "page" for each time you clear. These page tokens can be exchanged for the same gear or augmentation items that drop from the raids. How many required depends on the type of gear or item it is and ranges from 4 for accessories to 8 for chest and weapon.

Toward the end of each tier which will be at the tail end of each odd numbered patch, the loot restrictions will be lifted and you will be able to farm any of the fights to your heart's content.
