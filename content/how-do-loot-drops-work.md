---
authors:
    - Reiko
tags: 
- Loot
- Need
- Greed
---
# How do loot drops work? What is Need vs Greed?

## Short Answer

When loot pops up in the loot window during a duty, everyone in the party is given a chance to "roll" (i.e. get a random number of 1-99, highest number wins) on those pieces of loot. The 3 options are "Need", "Greed", and "Pass". Need always beats Greed. Anyone can Greed, but to Need, the gear must be appropriate for the job you are in that duty. Anyone can Need on non-gear items. **Leaving a duty before loot is given out is equivalent to passing on everything left, even if you rolled on it before leaving.**

## Extra Info 

- In addition to the loot that is rolled on by all party members, some loot will drop directly into your inventory. This can include random items like dyes and crystals, Materia, Triple Triad cards, or tokens meant to be exchanged for other items. Also, in leveling dungeons (i.e. all dungeons that are not level 50, 60, 70, or 80) a piece of gear from that dungeon's loot table you do not possess and that is appropriate for your job will drop directly into your inventory.
- If you already rolled on an item but later decide to pass, you can do so. However, once you have passed on an item that cannot be changed.
- Loot windows last for a maximum of 5 minutes, if you have not rolled on everything by then you will automatically pass.
- Do not feel bad about Greeding on loot! You participated in the duty, you deserve it as much as anyone. Also, feel free to ask your party members to pass on a specific piece of gear if you need it for glam or other specific purpose. Most people will be happy to accommodate a request like that.

## See Also

[How does loot work for savage raids?](max-level/How-does-loot-work-in-savage-raids.md)
