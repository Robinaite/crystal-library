---
authors:
        - Robin Naite
tags:
    - lvl49
---
# My next quest is level 49, what do I do?

## Short Answer

If you are stuck with your level you can do the following:

- Duty Roulette: Leveling (only once a day)
- Unlock Dzameal Darkhold Side-dungeon - [https://ffxiv.gamerescape.com/wiki/Fort_of_Fear](https://ffxiv.gamerescape.com/wiki/Fort_of_Fear){target=_blank}
- Unlock Aurum Vale side-dungeon - [https://ffxiv.gamerescape.com/wiki/Going_for_Gold](https://ffxiv.gamerescape.com/wiki/Going_for_Gold){target=_blank}
