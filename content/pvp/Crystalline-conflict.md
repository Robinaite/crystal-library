---
authors:
    - Robin Naite # Substitute my name
tags: # tags are to help categorize and search for specific stuff easier, as well as different ways to reference to this topic. For more check the guidelines document.
    - Crystalline Conflict
    - PvP
---
# Crystalline Conflict

## Summary

Crystalline Conflict is a "Push the Payload" style game mode with 2 teams, each one of them consisting of 5 players. There is a casual solo queue mode, and a ranked solo queue mode.

The objective is to push the Crystal that is in the middle all the way to the opponents base.

For more information you can check here: [FFXIV Lodestone Crystalline Conflict](https://na.finalfantasyxiv.com/lodestone/playguide/contentsguide/crystallineconflict/)

### Arenas
Crystalline Conflict has 3 maps that cycle on an 1 hour and 30 minutes timer. You can find what the current map, and the next map is by reading the duty description.
Every map has an unique feature, and a slightly different layout.

#### The Palaistra
![Palaistra](images/palaistra-pvp-crystalline-conflict-map.jpg)
The Palaistra is the most simplest map of them, consisting of two sprint zones, where swift sprint is always active.

#### The Volcanic Heart
![The Volcanic Heart map](images/volcanic-heart-pvp-crystalline-conflict-map.jpg)
The volcanic Heart is the most cramped map. It has Sprint zones similar to THe Palaistra, but also an Eruption Event.

The eruption event spawns bombs across the map, which will explode in the lines its in.
After the explosion goes off, they drop a bomb core, which if picked up gives the following effects:
- Increase Damage buff
- Boost to Limit Break bar

!!! info "Snare the others, and Guard yourself!"
    During eruption a good strategy is to try and snare/stun people in the explosion areas. The damage is considerable. If this happens to you, use Guard to take less damage!

#### Cloud Nine
![cloud nine map](images/cloud-nine-pvp-crystalline-conflict-map.jpg)

Cloud nine is the most open map of the three. It has Jump glyphs which help you getting from one area to another. It also has a Turbulence event. 

The Turbulence event happens after certain period, with a countdown. When the countdown goes off everyone will be thrown into the air and fall, taking fall damage.

There are two ways to prevent taking fall damage:
1. Use Guard when it happens
2. Stand on an orange circle with a chocobo face. This will not only remove fall damage, but also give a damage up, and limit break gauge up buff.

### Ranked
This game mode has a ranked ladder. The ranked ladder works with a tiering system. Every tier has a certain amount of Risers (minus Crystal Tier). Every Riser has 3 stars. The ranks are reset between PvP seasons.

| Tier | Risers |
|-----|--------|
| Bronze | 3 |
| Silver | 3 |
| Gold | 4 |
| Platinum | 4 |
| Diamond | 5 |
| Crystal |  |


#### Climbing the Ranked Ladder

To climb the ladder you will need to get Rising Stars. These are obtained by winning a game. You move up a Riser when you win with 3 stars. Afterwards, you move up a tier if you win with 3 stars, and are on the highest previous tier Riser. (For example, if you win a game in Bronze 1, you will rank up to Silver 3) You can also get 2 stars after every win, if you are on a winning streak, which is applied after you win 3 matches.

However, if you lose a game, you will lose a star. If you lose a game when having 0 stars, you will drop down to the previous riser and lose a star immediately.

**You will never drop down to a previous tier.**

#### Crystal Tier
Starting from the Crystal Tier, you will no longer have risers, and rather follow a credit system. 

You gain or lose a certain amount of credit based upon the difference in average credit between the two teams. 
