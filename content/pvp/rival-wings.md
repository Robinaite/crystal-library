---
authors:
    - Robin Naite # Substitute my name
tags: # tags are to help categorize and search for specific stuff easier, as well as different ways to reference to this topic. For more check the guidelines document.
    - Rival Wings
    - PvP
---
# Rival Wings

## Summary

Rival Wings is a large-scale PvP battle involving up to 48 combatants. Participants are divided into two teams, the Falcons and the Ravens, comprised of six light parties each.

You can find more information regarding Rival Wings here: [https://na.finalfantasyxiv.com/lodestone/playguide/contentsguide/rivalwings/](https://na.finalfantasyxiv.com/lodestone/playguide/contentsguide/rivalwings/){target=_blank}

#### Astragalos

During a Hidden Gorge campaign, teams vie for control of supplies delivered by ceruleum engines, and winning by destroying the core located on the opponent's base. (Think of League of Legends or DOTA)

More information: [https://na.finalfantasyxiv.com/lodestone/playguide/contentsguide/rivalwings/astragalos/](https://na.finalfantasyxiv.com/lodestone/playguide/contentsguide/rivalwings/astragalos/){target=_blank}