---
authors:
    - Robin Naite # Substitute my name
tags: # tags are to help categorize and search for specific stuff easier, as well as different ways to reference to this topic. For more check the guidelines document.
    - Feast
---
# The Feast

## Summary

*The Feast gamemode got removed since patch **6.1***

The Feast is a PvP arena in which the main objective is to steal your opponent's medals. The winner of these matches is determined by whichever team has the most at the end of each match. Individual PvP rating and tiers will also be assigned to players and the highest-ranked participants of each season will be awarded a special prize.

You can find more information regarding The Feast here: [https://na.finalfantasyxiv.com/lodestone/playguide/contentsguide/wolvesden/thefeast/](https://na.finalfantasyxiv.com/lodestone/playguide/contentsguide/wolvesden/thefeast/){target=_blank}