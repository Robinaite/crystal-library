---
authors:
        - Robin Naite
tags:
    - PvP
    - Crystaline Conflict
    - Frontline
    - Rival Wings
    - Feast
---
# PvP in FFXIV

## Summary

Player Vs Player in FFXIV uses a completely different skills and abilities set than the PvE content. There are various game modes, all of which give wolf marks and series maimstones, which can be exchanged for rewards. Crystaline Conflict also has a Ranked mode.

- [Unlock PvP](unlock-pvp.md)

## Game Modes

There are various PvP modes in the game:

- [Crystaline Conflict](Crystalline-conflict.md) - 5v5 - Push the payload style matches
- [Frontline](frontline.md) - 24v24v24 Capture The Flag style matches
- [Rival Wings](rival-wings.md) - 24v24 MOBA styled matches

The Feast game mode is no longer in the game, you can still read information about it here: [Feast](feast.md)

### See Also
- FFXIV PvP Guide: [https://na.finalfantasyxiv.com/lodestone/playguide/pvpguide/system/](https://na.finalfantasyxiv.com/lodestone/playguide/pvpguide/system/){target=_blank}
- Revival Wings Discord -  [https://discord.gg/WYMpfYp](https://discord.gg/WYMpfYp){target=_blank}
- PvPaissa Discord - [https://discord.gg/sUy86UC](https://discord.gg/sUy86UC){target=_blank}
