---
authors:
    - Robin Naite # Substitute my name
tags: # tags are to help categorize and search for specific stuff easier, as well as different ways to reference to this topic. For more check the guidelines document.
    - Frontline
    - PvP
---
# Frontline

## Summary

!!! info "Amazing XP"
    Did you know you can get a lot of XP out of doing Frontline Roulette, once a day? You can even swap inside the PvP zone, and you will still get xp on the job you queued with!!

Frontline is a 3 team Capture The Flag style fight, where each team represents one of the Grand Companies. Each team consists of 24 players (3x 8 people).
Frontline has various maps with slightly different rules and spins on the Capture the Flag style game.

You can find more information regarding Frontline here: [https://na.finalfantasyxiv.com/lodestone/playguide/contentsguide/frontline/](https://na.finalfantasyxiv.com/lodestone/playguide/contentsguide/frontline/){target=_blank}

### Maps

Frontline is constituted of 4 different maps:

- **The Borderland Ruins (Secure)** - players increase their company's tactical rating by occupying and maintaining control of key locations, as well as defeating other players in battle.
- **Seal Rock (Seize)** - layers aid their company by retrieving data from Allagan tomeliths, as well as defeating other players in battle.
- **The Fields of Glory (Shatter)** - players aid their company by retrieving data from Allagan tomeliths, as well as defeating other players in battle.
- **Onsal Hakair (Danshig Naadam)** -  players increase their team's tactical rating by defeating enemy players, as well as claiming ovoos (flags).