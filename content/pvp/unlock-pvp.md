---
authors:
    - Robin Naite # Substitute my name
tags: # tags are to help categorize and search for specific stuff easier, as well as different ways to reference to this topic. For more check the guidelines document.
    - Unlock
    - PvP
---
# Unlock PvP

## Summary

You can unlock PvP zone The Wolves' Den at one of your Grand Companies through the quest "A Pup no Longer".

To unlock Frontline & respective roulette do the quest "Like Civilized Men and Women" at your grand company

To unlock Rival Wings do the quest "Earning your Wings".